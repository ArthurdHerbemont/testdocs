.. code-block:: aimms

    ElementRange(lksndv,sdvs);
    P_Parameter := V_Variable;


LibraryModule AutoDoc
#####################

.. aimms:librarymodule:: AutoDoc

    :attribute Prefix: ``ad``

    :attribute Interface: `Section GeneralPublic`_

.. aimms:module:: ad



Section GeneralPublic
*********************


.. aimms:set:: FileNumbers

    :attribute SubsetOf: :aimms:set:`Integers`

    :attribute Index: (:aimms:index:`fn`)

    :attribute Parameter: EP_FileNumber


    .. aimms:index:: fn

        :attribute Range: :aimms:set:`FileNumbers`


.. aimms:stringparameter:: AMSFiles

    :attribute IndexDomain: (:aimms:index:`fn`)


.. aimms:stringparameter:: index_template


.. aimms:stringparameter:: path_to_sphinx_build



.. aimms:procedure:: BuildAndOpen


.. aimms:procedure:: AddAMSFile



    Add a path for an AMS file to be documented. This function adds the path to :aimms:stringparameter:`ad::AMSFiles`.
    
    Args:
    
    :argument file_to_add: path to the AMS file to add to :aimms:stringparameter:`ad::AMSFiles`
    :type file_to_add: str
    
    :returns: a new Path in :aimms:stringparameter:`ad::AMSFiles`


    .. aimms:stringparameter:: file_to_add
    
        :attribute Property: Input
    
    

Section GeneralPrivate
**********************


.. aimms:stringparameter:: FileNames

    :attribute IndexDomain: (:aimms:index:`fn`)


.. aimms:procedure:: GenerateRST



    From a list of AMS files, generates accordingly a list of RST files, 
    and move them in the Sphinx HTML_Generator repo (Autodoc/HTML_Generator/). (for further building of the HTML)
    
    :attribute stringparameter path_to_python: Path to your local python installation
    
    :attribute parameter whatever: doing whatever


    .. aimms:stringparameter:: path_to_python
    
        :attribute Property: Input
    
    
.. aimms:procedure:: BuildDoc



    .. aimms:stringparameter:: output_format
    
        :attribute Property: Input
    
    
    .. aimms:stringparameter:: output_folder
    
        :attribute Property: Input
    
    
    .. aimms:stringparameter:: path_to_sphinx
    
        :attribute Property: Input
    
    
.. aimms:procedure:: OpenDoc



    .. aimms:stringparameter:: file_to_open
    
        :attribute Property: Input
    
    
.. aimms:procedure:: BuildSphinxIndex


    .. aimms:file:: indexRST
    
        :attribute Name: "AutoDoc\\HTML_Generator\\index.rst"
    
        :attribute Device: Disk
    
        :attribute Mode: replace
    
    
.. aimms:procedure:: LibraryInitialization



    Add initialization statements here that do not require any other library being initialized already.

.. aimms:procedure:: LibraryTermination


    Add termination statements here that do not require other libraries to be still alive.
    Return 1 to allow the termination sequence to continue.
    Return 0 if you want to cancel the termination sequence.
    It is recommended to only use the procedure PreLibraryTermination to cancel the termination sequence and let this procedure always return 1.

