

Model Main Sphinx
#################


.. aimms:procedure:: MainInitialization



    Add initialization statements here that do NOT require any library being initialized already.

.. aimms:procedure:: PostMainInitialization


    Add initialization statements here that require that the libraries are already initialized properly,
    or add statements that require the Data Management module to be initialized.


.. aimms:procedure:: MainExecution


.. aimms:procedure:: PreMainTermination


    Add termination statements here that require all libraries to be still alive.
    Return 1 if you allow the termination sequence to continue.
    Return 0 if you want to cancel the termination sequence.


.. aimms:procedure:: MainTermination


    Add termination statements here that do not require all libraries to be still alive.
    Return 1 to allow the termination sequence to continue.
    Return 0 if you want to cancel the termination sequence.
    It is recommended to only use the procedure PreMainTermination to cancel the termination sequence and let this procedure always return 1.

