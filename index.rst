﻿.. book-test documentation master file, created by
   sphinx-quickstart on Tue Oct 16 10:45:39 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome Autodoc and more documentation!
==============================================


:doc:`optimization-modeling-components/implementing-advanced-algorithms-for-mathematical-programs/index`


.. raw:: html

  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: 'Company Performance',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="curve_chart" style="width: 900px; height: 500px"></div>
  </body>
  
.. toctree::

    Sphinx
    AutoDoc
    hypergeometric
    PredeclaredIdentifiers
    numerical-expressions


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
