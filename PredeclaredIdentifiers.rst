

LibraryModule Predeclared Identifiers
#####################################

.. aimms:librarymodule:: Predeclared_Identifiers



.. aimms:set:: AllSymbols

    :attribute Text: All symbols declared for this model, including AllIdentifiers, AllPredeclaredIdentifiers, AllKeywords and AllIntrinsics

    :attribute Index: (:aimms:index:`IndexSymbols`)


    .. aimms:index:: IndexSymbols

        :attribute Range: :aimms:set:`AllSymbols`


.. aimms:set:: AllAimmsStringConstantElements

    :attribute Text: All elements that, together, are used to indicate a particular installation of AIMMS

    :attribute Index: (:aimms:index:`IndexAimmsStringConstantElements`)


    .. aimms:index:: IndexAimmsStringConstantElements

        :attribute Range: :aimms:set:`AllAimmsStringConstantElements`


.. aimms:stringparameter:: AimmsStringConstants

    :attribute IndexDomain: (:aimms:index:`IndexAimmsStringConstantElements`)

    :attribute Text: Aimms configuration values depending on installation


.. aimms:set:: AllOptions

    :attribute Text: All Options available in the option tree

    :attribute Index: (:aimms:index:`IndexOptions`)


    .. aimms:index:: IndexOptions

        :attribute Range: :aimms:set:`AllOptions`


.. aimms:set:: AllPredeclaredIdentifiers

    :attribute SubsetOf: :aimms:set:`AllSymbols`

    :attribute Text: All predeclared identifiers in the model

    :attribute Index: (:aimms:index:`IndexPredeclaredIdentifiers`)


    .. aimms:index:: IndexPredeclaredIdentifiers

        :attribute Range: :aimms:set:`AllPredeclaredIdentifiers`


.. aimms:set:: AllKeywords

    :attribute SubsetOf: :aimms:set:`AllSymbols`

    :attribute Text: All keywords available to the current model

    :attribute Index: (:aimms:index:`IndexKeywords`)


    .. aimms:index:: IndexKeywords

        :attribute Range: :aimms:set:`AllKeywords`


.. aimms:set:: AllSolvers

    :attribute Text: All available solvers in this solver configuration

    :attribute Index: (:aimms:index:`IndexSolvers`)


    .. aimms:index:: IndexSolvers

        :attribute Range: :aimms:set:`AllSolvers`


.. aimms:set:: AllGMPExtensions

    :attribute Text: All used extended generation types

    :attribute Index: (:aimms:index:`IndexGMPExtensions`)


    .. aimms:index:: IndexGMPExtensions

        :attribute Range: :aimms:set:`AllGMPExtensions`


.. aimms:set:: AllColors

    :attribute Text: All colors present within this project

    :attribute Index: (:aimms:index:`IndexColors`)


    .. aimms:index:: IndexColors

        :attribute Range: :aimms:set:`AllColors`


.. aimms:set:: AllCharacterEncodings

    :attribute Text: All character encodings

    :attribute Index: (:aimms:index:`IndexCharacterEncodings`)


    .. aimms:index:: IndexCharacterEncodings

        :attribute Range: :aimms:set:`AllCharacterEncodings`


.. aimms:set:: ASCIICharacterEncodings

    :attribute SubsetOf: :aimms:set:`AllCharacterEncodings`

    :attribute Text: Character encodings that share the first 126 code points with US-ASCII encoding

    :attribute Index: (:aimms:index:`IndexASCIICharacterEncodings`)


    .. aimms:index:: IndexASCIICharacterEncodings

        :attribute Range: :aimms:set:`ASCIICharacterEncodings`


.. aimms:set:: UnicodeCharacterEncodings

    :attribute SubsetOf: :aimms:set:`AllCharacterEncodings`

    :attribute Text: UNICODE character encodings

    :attribute Index: (:aimms:index:`IndexUnicodeCharacterEncodings`)


    .. aimms:index:: IndexUnicodeCharacterEncodings

        :attribute Range: :aimms:set:`UnicodeCharacterEncodings`


.. aimms:set:: ASCIIUnicodeCharacterEncodings

    :attribute SubsetOf: :aimms:set:`AllCharacterEncodings`

    :attribute Text: Union of ASCIICharacterEncodings and UnicodeCharacterEncodings

    :attribute Index: (:aimms:index:`IndexASCIIUnicodeCharacterEncodings`)


    .. aimms:index:: IndexASCIIUnicodeCharacterEncodings

        :attribute Range: :aimms:set:`ASCIIUnicodeCharacterEncodings`


.. aimms:set:: AllAvailableCharacterEncodings

    :attribute SubsetOf: :aimms:set:`AllCharacterEncodings`

    :attribute Text: All Available character encodings on this system

    :attribute Index: (:aimms:index:`IndexAvailableCharacterEncodings`)


    .. aimms:index:: IndexAvailableCharacterEncodings

        :attribute Range: :aimms:set:`AllAvailableCharacterEncodings`


.. aimms:set:: AllAuthorizationLevels

    :attribute Text: All possible authorization levels

    :attribute Index: (:aimms:index:`IndexAuthorizationLevels`)


    .. aimms:index:: IndexAuthorizationLevels

        :attribute Range: :aimms:set:`AllAuthorizationLevels`


.. aimms:elementparameter:: CurrentAuthorizationLevel

    :attribute Text: The authorization level of the current user/group

    :attribute Range: :aimms:set:`AllAuthorizationLevels`


.. aimms:stringparameter:: CurrentUser

    :attribute Text: The user that is currently logged into this project


.. aimms:stringparameter:: CurrentGroup

    :attribute Text: The group to which the current user currently belongs


.. aimms:parameter:: ProfilerData

    :attribute IndexDomain: (:aimms:index:`IndexIdentifiers`, :aimms:index:`IndexProfilerTypes`)

    :attribute Text: Registers profiler data for procedures, functions and definitions


.. aimms:elementparameter:: CurrentSolver

    :attribute IndexDomain: (:aimms:index:`IndexMathematicalProgrammingTypes`)

    :attribute Text: The solver that will be used for the next solve

    :attribute Range: :aimms:set:`AllSolvers`


.. aimms:elementparameter:: CurrentDefaultCaseType

    :attribute Text: The case type used when saving a case

    :attribute Range: :aimms:set:`AllCaseTypes`



.. aimms:set:: AggregationTypes

    :attribute SubsetOf: :aimms:set:`AllValueKeywords`

    :attribute Text: The operation types available to the intrinsic procedures Aggregate and Disaggregate

    :attribute Index: (:aimms:index:`IndexAggregationTypes`)


    .. aimms:index:: IndexAggregationTypes

        :attribute Range: :aimms:set:`AggregationTypes`


.. aimms:set:: AllAttributeNames

    :attribute Text: All attribute names supported by this release

    :attribute Index: (:aimms:index:`IndexAttributeNames`)


    .. aimms:index:: IndexAttributeNames

        :attribute Range: :aimms:set:`AllAttributeNames`


.. aimms:set:: AllColumnTypes

    :attribute Text: All possible column types

    :attribute Index: (:aimms:index:`IndexColumnTypes`)


    .. aimms:index:: IndexColumnTypes

        :attribute Range: :aimms:set:`AllColumnTypes`


.. aimms:set:: AllIdentifierTypes

    :attribute Text: The types of the identifiers that are elements in the set AllIdentifiers

    :attribute Index: (:aimms:index:`IndexIdentifierTypes`)


    .. aimms:index:: IndexIdentifierTypes

        :attribute Range: :aimms:set:`AllIdentifierTypes`


.. aimms:set:: AllIsolationLevels

    :attribute Text: All database Transaction Isolation Levels

    :attribute Index: (:aimms:index:`IndexIsolationLevels`)


    .. aimms:index:: IndexIsolationLevels

        :attribute Range: :aimms:set:`AllIsolationLevels`


.. aimms:set:: AllFileAttributes

    :attribute Text: All file attributes that can be used to filter files and subdirectories via DirectoryGetFiles and DirectoryGetSubdirectories

    :attribute Index: (:aimms:index:`IndexFileAttributes`)


    .. aimms:index:: IndexFileAttributes

        :attribute Range: :aimms:set:`AllFileAttributes`


.. aimms:set:: AllDataColumnCharacteristics

    :attribute Text: All SQL Data column characteristics available to the function SQLColumnData

    :attribute Index: (:aimms:index:`IndexDataColumnCharacteristics`)


    .. aimms:index:: IndexDataColumnCharacteristics

        :attribute Range: :aimms:set:`AllDataColumnCharacteristics`


.. aimms:set:: AllDatabaseInterfaces

    :attribute Text: All database interfaces supported by AIMMS

    :attribute Index: (:aimms:index:`IndexDatabaseInterfaces`)


    .. aimms:index:: IndexDatabaseInterfaces

        :attribute Range: :aimms:set:`AllDatabaseInterfaces`


.. aimms:set:: AllDatasourceProperties

    :attribute Text: All datasource properties that can be queried using the function GetDatasourceProperties

    :attribute Index: (:aimms:index:`IndexDatasourceProperties`)


    .. aimms:index:: IndexDatasourceProperties

        :attribute Range: :aimms:set:`AllDatasourceProperties`


.. aimms:set:: AllMathematicalProgrammingTypes

    :attribute SubsetOf: :aimms:set:`AllValueKeywords`

    :attribute Text: All available mathematical programming types

    :attribute Index: (:aimms:index:`IndexMathematicalProgrammingTypes`)


    .. aimms:index:: IndexMathematicalProgrammingTypes

        :attribute Range: :aimms:set:`AllMathematicalProgrammingTypes`


.. aimms:set:: AllMatrixManipulationDirections

    :attribute SubsetOf: :aimms:set:`AllValueKeywords`

    :attribute Text: Select between the values Maximizing, Minimizing and none

    :attribute Index: (:aimms:index:`IndexMatrixManipulationDirections`)


    .. aimms:index:: IndexMatrixManipulationDirections

        :attribute Range: :aimms:set:`AllMatrixManipulationDirections`


.. aimms:set:: AllMatrixManipulationProgrammingTypes

    :attribute SubsetOf: :aimms:set:`AllMathematicalProgrammingTypes`

    :attribute Text: All mathematical programming types that can be handled by the matrix manipulation functions

    :attribute Index: (:aimms:index:`IndexMatrixManipulationProgrammingTypes`)


    .. aimms:index:: IndexMatrixManipulationProgrammingTypes

        :attribute Range: :aimms:set:`AllMatrixManipulationProgrammingTypes`


.. aimms:set:: AllRowColumnStatuses

    :attribute Text: All possible status values of a row or column in a GMP

    :attribute Index: (:aimms:index:`IndexRowColumnStatusess`)


    .. aimms:index:: IndexRowColumnStatusess

        :attribute Range: :aimms:set:`AllRowColumnStatuses`


.. aimms:set:: AllRowTypes

    :attribute Text: All possible row types

    :attribute Index: (:aimms:index:`IndexRowTypes`)


    .. aimms:index:: IndexRowTypes

        :attribute Range: :aimms:set:`AllRowTypes`


.. aimms:set:: AllMathematicalProgrammingRowTypes

    :attribute SubsetOf: :aimms:set:`AllRowTypes`

    :attribute Text: All possible mathematical programming row types

    :attribute Index: (:aimms:index:`IndexMathematicalProgrammingRowTypes`)


    .. aimms:index:: IndexMathematicalProgrammingRowTypes

        :attribute Range: :aimms:set:`AllMathematicalProgrammingRowTypes`


.. aimms:set:: AllConstraintProgrammingRowTypes

    :attribute SubsetOf: :aimms:set:`AllRowTypes`

    :attribute Text: All possible constraint programming row types

    :attribute Index: (:aimms:index:`IndexConstraintProgrammingRowTypes`)


    .. aimms:index:: IndexConstraintProgrammingRowTypes

        :attribute Range: :aimms:set:`AllConstraintProgrammingRowTypes`


.. aimms:set:: AllSolutionStates

    :attribute Text: All possible solution states returned by a solver

    :attribute Index: (:aimms:index:`IndexSolutionStates`)


    .. aimms:index:: IndexSolutionStates

        :attribute Range: :aimms:set:`AllSolutionStates`


.. aimms:set:: AllBasicValues

    :attribute Text: All possible basic status values of variables and constraints

    :attribute Index: (:aimms:index:`IndexBasicValues`)


    .. aimms:index:: IndexBasicValues

        :attribute Range: :aimms:set:`AllBasicValues`


.. aimms:set:: AllChanceApproximationTypes

    :attribute Text: All possible chance approximation types for chance constraints

    :attribute Index: (:aimms:index:`IndexChanceApproximationTypes`)


    .. aimms:index:: IndexChanceApproximationTypes

        :attribute Range: :aimms:set:`AllChanceApproximationTypes`


.. aimms:set:: AllSolverInterrupts

    :attribute Text: All possible causes for a solver to suspend (temporarily) execution

    :attribute Index: (:aimms:index:`IndexSolverInterrupts`)


    .. aimms:index:: IndexSolverInterrupts

        :attribute Range: :aimms:set:`AllSolverInterrupts`


.. aimms:set:: AllExecutionStatuses

    :attribute Text: Indicates whether an asynchronous solve is pending, executing or finished

    :attribute Index: (:aimms:index:`IndexExecutionStatus`)


    .. aimms:index:: IndexExecutionStatus

        :attribute Range: :aimms:set:`AllExecutionStatuses`


.. aimms:set:: AllStochasticGenerationModes

    :attribute Text: All generation modes for stochastic mathematical programs

    :attribute Index: (:aimms:index:`IndexStochasticGenerationModes`)


    .. aimms:index:: IndexStochasticGenerationModes

        :attribute Range: :aimms:set:`AllStochasticGenerationModes`


.. aimms:set:: AllIntrinsics

    :attribute SubsetOf: :aimms:set:`AllSymbols`

    :attribute Text: All intrinsic functions and procedures of AIMMS

    :attribute Index: (:aimms:index:`IndexIntrinsics`)


    .. aimms:index:: IndexIntrinsics

        :attribute Range: :aimms:set:`AllIntrinsics`


.. aimms:set:: AllProfilerTypes

    :attribute Text: All profiler types for which data is registered in ProfilerData

    :attribute Index: (:aimms:index:`IndexProfilerTypes`)


    .. aimms:index:: IndexProfilerTypes

        :attribute Range: :aimms:set:`AllProfilerTypes`


.. aimms:set:: AllSuffixNames

    :attribute Text: All suffix names documented in the AIMMS 3 language reference

    :attribute Index: (:aimms:index:`IndexSuffixNames`)


    .. aimms:index:: IndexSuffixNames

        :attribute Range: :aimms:set:`AllSuffixNames`


.. aimms:set:: AllDifferencingModes

    :attribute Text: All differencing modes supported by the intrinsic procedure "CaseCreateDifferenceFile

    :attribute Index: (:aimms:index:`IndexDifferencingModes`)


    .. aimms:index:: IndexDifferencingModes

        :attribute Range: :aimms:set:`AllDifferencingModes`


.. aimms:set:: AllCaseComparisonModes

    :attribute Text: All comparison modes supported by the intrinsic procedure "CaseCompareIdentifier

    :attribute Index: (:aimms:index:`IndexCaseComparisonModes`)


    .. aimms:index:: IndexCaseComparisonModes

        :attribute Range: :aimms:set:`AllCaseComparisonModes`


.. aimms:set:: AllValueKeywords

    :attribute Text: All relevant keywords that can be used as an element value

    :attribute Index: (:aimms:index:`IndexValueKeywords`)


    .. aimms:index:: IndexValueKeywords

        :attribute Range: :aimms:set:`AllValueKeywords`


.. aimms:set:: AllViolationTypes

    :attribute SubsetOf: :aimms:set:`AllValueKeywords`

    :attribute Text: The violation types lower, upper and definition

    :attribute Index: (:aimms:index:`IndexViolationTypes`)


    .. aimms:index:: IndexViolationTypes

        :attribute Range: :aimms:set:`AllViolationTypes`


.. aimms:set:: ContinueAbort

    :attribute SubsetOf: :aimms:set:`AllValueKeywords`

    :attribute Text: All possible action codes for the mathematical program callback functions

    :attribute Index: (:aimms:index:`IndexContinueAbort`)


    .. aimms:index:: IndexContinueAbort

        :attribute Range: :aimms:set:`ContinueAbort`


.. aimms:set:: DiskWindowVoid

    :attribute SubsetOf: :aimms:set:`AllValueKeywords`

    :attribute Text: All output types for files

    :attribute Index: (:aimms:index:`IndexDiskWindowVoid`)


    .. aimms:index:: IndexDiskWindowVoid

        :attribute Range: :aimms:set:`DiskWindowVoid`


.. aimms:set:: Integers

    :attribute Text: integers used as elements

    :attribute Index: (:aimms:index:`IndexIntegers`)


    .. aimms:index:: IndexIntegers

        :attribute Range: :aimms:set:`Integers`


.. aimms:set:: MaximizingMinimizing

    :attribute SubsetOf: :aimms:set:`AllValueKeywords`

    :attribute Text: Select between the values Maximizing and Minimizing

    :attribute Index: (:aimms:index:`IndexMaximizingMinimizing`)


    .. aimms:index:: IndexMaximizingMinimizing

        :attribute Range: :aimms:set:`MaximizingMinimizing`


.. aimms:set:: CreatePresolvedLevels

    :attribute Text: Select between different amounts of work to be done by the function gmp::instance::CreatePresolved

    :attribute Index: (:aimms:index:`IndexCreatePresolvedLevels`)


    .. aimms:index:: IndexCreatePresolvedLevels

        :attribute Range: :aimms:set:`CreatePresolvedLevels`


.. aimms:set:: MergeReplace

    :attribute SubsetOf: :aimms:set:`AllValueKeywords`

    :attribute Text: Select between the values MERGE and REPLACE

    :attribute Index: (:aimms:index:`IndexMergeReplace`)


    .. aimms:index:: IndexMergeReplace

        :attribute Range: :aimms:set:`MergeReplace`


.. aimms:set:: DatabaseWriteModes

    :attribute SubsetOf: :aimms:set:`AllValueKeywords`

    :attribute Text: Select between the various modes for writing to a database table

    :attribute Index: (:aimms:index:`IndexDatabaseWriteModes`)


    .. aimms:index:: IndexDatabaseWriteModes

        :attribute Range: :aimms:set:`DatabaseWriteModes`


.. aimms:set:: OnOff

    :attribute SubsetOf: :aimms:set:`AllValueKeywords`

    :attribute Text: All possible values for a toggle

    :attribute Index: (:aimms:index:`IndexOnOff`)


    .. aimms:index:: IndexOnOff

        :attribute Range: :aimms:set:`OnOff`


.. aimms:set:: TimeslotCharacteristics

    :attribute Text: All Timeslot characteristics available to the function TimeslotCharacteristic

    :attribute Index: (:aimms:index:`IndexTimeslotCharacteristics`)


    .. aimms:index:: IndexTimeslotCharacteristics

        :attribute Range: :aimms:set:`TimeslotCharacteristics`


.. aimms:set:: YesNo

    :attribute SubsetOf: :aimms:set:`AllValueKeywords`

    :attribute Text: Select between the values Yes and No

    :attribute Index: (:aimms:index:`IndexYesNo`)


    .. aimms:index:: IndexYesNo

        :attribute Range: :aimms:set:`YesNo`



.. aimms:set:: AllAssertions

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All assertions in the model

    :attribute Index: (:aimms:index:`IndexAssertions`)


    .. aimms:index:: IndexAssertions

        :attribute Range: :aimms:set:`AllAssertions`


.. aimms:set:: AllConstraints

    :attribute SubsetOf: :aimms:set:`AllVariablesConstraints`

    :attribute Text: All constraints and defined variables in the model

    :attribute Index: (:aimms:index:`IndexConstraints`)


    .. aimms:index:: IndexConstraints

        :attribute Range: :aimms:set:`AllConstraints`


.. aimms:set:: AllConventions

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All conventions in the model

    :attribute Index: (:aimms:index:`IndexConventions`)


    .. aimms:index:: IndexConventions

        :attribute Range: :aimms:set:`AllConventions`


.. aimms:set:: AllDatabaseTables

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All databases in the model

    :attribute Index: (:aimms:index:`IndexDatabaseTables`)


    .. aimms:index:: IndexDatabaseTables

        :attribute Range: :aimms:set:`AllDatabaseTables`


.. aimms:set:: AllDefinedParameters

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All defined parameters in the model

    :attribute Index: (:aimms:index:`IndexDefinedParameters`)


    .. aimms:index:: IndexDefinedParameters

        :attribute Range: :aimms:set:`AllDefinedParameters`


.. aimms:set:: AllDefinedSets

    :attribute SubsetOf: :aimms:set:`AllSets`

    :attribute Text: All defined sets in the model

    :attribute Index: (:aimms:index:`IndexDefinedSets`)


    .. aimms:index:: IndexDefinedSets

        :attribute Range: :aimms:set:`AllDefinedSets`


.. aimms:set:: AllFiles

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All files in the model

    :attribute Index: (:aimms:index:`IndexFiles`)


    .. aimms:index:: IndexFiles

        :attribute Range: :aimms:set:`AllFiles`


.. aimms:set:: AllFunctions

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All functions in the model

    :attribute Index: (:aimms:index:`IndexFunctions`)


    .. aimms:index:: IndexFunctions

        :attribute Range: :aimms:set:`AllFunctions`


.. aimms:set:: AllGeneratedMathematicalPrograms

    :attribute Text: All generated mathematical programs constructed during this session

    :attribute Index: (:aimms:index:`IndexGeneratedMathematicalPrograms`)


    .. aimms:index:: IndexGeneratedMathematicalPrograms

        :attribute Range: :aimms:set:`AllGeneratedMathematicalPrograms`


.. aimms:set:: AllIdentifiers

    :attribute SubsetOf: :aimms:set:`AllSymbols`

    :attribute Text: All global identifiers in the model

    :attribute Index: (:aimms:index:`IndexIdentifiers`, :aimms:index:`SecondIndexIdentifiers`)


    .. aimms:index:: IndexIdentifiers

        :attribute Range: :aimms:set:`AllIdentifiers`

    .. aimms:index::  SecondIndexIdentifiers

        :attribute Range: :aimms:set:`AllIdentifiers`


.. aimms:set:: AllIndices

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All indices in the model

    :attribute Index: (:aimms:index:`IndexIndices`)


    .. aimms:index:: IndexIndices

        :attribute Range: :aimms:set:`AllIndices`


.. aimms:set:: AllIntegerVariables

    :attribute SubsetOf: :aimms:set:`AllVariables`

    :attribute Text: All integer variables in the model

    :attribute Index: (:aimms:index:`IndexIntegerVariables`)


    .. aimms:index:: IndexIntegerVariables

        :attribute Range: :aimms:set:`AllIntegerVariables`


.. aimms:set:: AllMacros

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All macros in the model

    :attribute Index: (:aimms:index:`IndexMacros`)


    .. aimms:index:: IndexMacros

        :attribute Range: :aimms:set:`AllMacros`


.. aimms:set:: AllMathematicalPrograms

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All symbolic mathematical programs in the model

    :attribute Index: (:aimms:index:`IndexMathematicalPrograms`)


    .. aimms:index:: IndexMathematicalPrograms

        :attribute Range: :aimms:set:`AllMathematicalPrograms`


.. aimms:set:: AllNonlinearConstraints

    :attribute SubsetOf: :aimms:set:`AllConstraints`

    :attribute Text: All nonlinear constraints in the model

    :attribute Index: (:aimms:index:`IndexNonlinearConstraints`)


    .. aimms:index:: IndexNonlinearConstraints

        :attribute Range: :aimms:set:`AllNonlinearConstraints`


.. aimms:set:: AllParameters

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All parameters in the model

    :attribute Index: (:aimms:index:`IndexParameters`)


    .. aimms:index:: IndexParameters

        :attribute Range: :aimms:set:`AllParameters`


.. aimms:set:: AllProcedures

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All procedures in the model

    :attribute Index: (:aimms:index:`IndexProcedures`)


    .. aimms:index:: IndexProcedures

        :attribute Range: :aimms:set:`AllProcedures`


.. aimms:set:: AllQuantities

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All quantities in the model

    :attribute Index: (:aimms:index:`IndexQuantities`)


    .. aimms:index:: IndexQuantities

        :attribute Range: :aimms:set:`AllQuantities`


.. aimms:set:: AllSections

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All sections in the model

    :attribute Index: (:aimms:index:`IndexSections`)


    .. aimms:index:: IndexSections

        :attribute Range: :aimms:set:`AllSections`


.. aimms:set:: AllSets

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All sets in the model

    :attribute Index: (:aimms:index:`IndexSets`)


    .. aimms:index:: IndexSets

        :attribute Range: :aimms:set:`AllSets`


.. aimms:set:: AllSolverSessionCompletionObjects

    :attribute Text: Encompassing root set of AllGMPEvents and AllSolverSessions

    :attribute Index: (:aimms:index:`IndexSolverSessionCompletionObjects`)


    .. aimms:index:: IndexSolverSessionCompletionObjects

        :attribute Range: :aimms:set:`AllSolverSessionCompletionObjects`


.. aimms:set:: AllSolverSessions

    :attribute SubsetOf: :aimms:set:`AllSolverSessionCompletionObjects`

    :attribute Text: All solver sessions explicitly created to solve a generated mathematical program

    :attribute Index: (:aimms:index:`IndexSolverSessions`)


    .. aimms:index:: IndexSolverSessions

        :attribute Range: :aimms:set:`AllSolverSessions`


.. aimms:set:: AllGMPEvents

    :attribute SubsetOf: :aimms:set:`AllSolverSessionCompletionObjects`

    :attribute Text: Wait for solver session completion events

    :attribute Index: (:aimms:index:`IndexGMPEvents`)


    .. aimms:index:: IndexGMPEvents

        :attribute Range: :aimms:set:`AllGMPEvents`


.. aimms:set:: AllStochasticParameters

    :attribute SubsetOf: :aimms:set:`AllParameters`

    :attribute Text: All parameters in the model with the property Stochastic set

    :attribute Index: (:aimms:index:`IndexStochasticParameters`)


    .. aimms:index:: IndexStochasticParameters

        :attribute Range: :aimms:set:`AllStochasticParameters`


.. aimms:set:: AllStochasticVariables

    :attribute SubsetOf: :aimms:set:`AllVariables`

    :attribute Text: All variables in the model with the property Stochastic set

    :attribute Index: (:aimms:index:`IndexStochasticVariables`)


    .. aimms:index:: IndexStochasticVariables

        :attribute Range: :aimms:set:`AllStochasticVariables`


.. aimms:set:: AllStochasticConstraints

    :attribute SubsetOf: :aimms:set:`AllVariablesConstraints`

    :attribute Text: All constraints in the model that reference a stochastic variable in their definition

    :attribute Index: (:aimms:index:`IndexStochasticConstraints`)


    .. aimms:index:: IndexStochasticConstraints

        :attribute Range: :aimms:set:`AllStochasticConstraints`


.. aimms:set:: AllSubsetsOfAllIdentifiers

    :attribute SubsetOf: :aimms:set:`AllSymbols`

    :attribute Text: All named subsets of AllIdentifiers in the model.

    :attribute Index: (:aimms:index:`IndexSubsetsOfAllIdentifiers`)


    .. aimms:index:: IndexSubsetsOfAllIdentifiers

        :attribute Range: :aimms:set:`AllSubsetsOfAllIdentifiers`


.. aimms:set:: AllUpdatableIdentifiers

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: The identifiers that can be made updatable

    :attribute Index: (:aimms:index:`IndexUpdatableIdentifiers`)


    .. aimms:index:: IndexUpdatableIdentifiers

        :attribute Range: :aimms:set:`AllUpdatableIdentifiers`


.. aimms:set:: AllUncertainParameters

    :attribute SubsetOf: :aimms:set:`AllParameters`

    :attribute Text: All uncertain parameters in the model

    :attribute Index: (:aimms:index:`IndexUncertainParameters`)


    .. aimms:index:: IndexUncertainParameters

        :attribute Range: :aimms:set:`AllUncertainParameters`


.. aimms:set:: AllUncertaintyConstraints

    :attribute SubsetOf: :aimms:set:`AllVariablesConstraints`

    :attribute Text: All uncertainty constraints in the model

    :attribute Index: (:aimms:index:`IndexUncertaintyConstraints`)


    .. aimms:index:: IndexUncertaintyConstraints

        :attribute Range: :aimms:set:`AllUncertaintyConstraints`


.. aimms:set:: AllVariables

    :attribute SubsetOf: :aimms:set:`AllVariablesConstraints`

    :attribute Text: All variables in the model

    :attribute Index: (:aimms:index:`IndexVariables`)


    .. aimms:index:: IndexVariables

        :attribute Range: :aimms:set:`AllVariables`


.. aimms:set:: AllVariablesConstraints

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: All constraints and variables in the model

    :attribute Index: (:aimms:index:`IndexVariablesConstraints`)


    .. aimms:index:: IndexVariablesConstraints

        :attribute Range: :aimms:set:`AllVariablesConstraints`



.. aimms:set:: AllProgressCategories

    :attribute Text: The progress categories displayed in the progress window

    :attribute Index: (:aimms:index:`IndexProgressCategories`)


    .. aimms:index:: IndexProgressCategories

        :attribute Range: :aimms:set:`AllProgressCategories`


.. aimms:set:: AllStochasticScenarios

    :attribute Text: The root set of all stochastic scenarios

    :attribute Index: (:aimms:index:`IndexStochasticScenarios`)


    .. aimms:index:: IndexStochasticScenarios

        :attribute Range: :aimms:set:`AllStochasticScenarios`


.. aimms:set:: CurrentAutoUpdatedDefinitions

    :attribute SubsetOf: :aimms:set:`AllIdentifiers`

    :attribute Text: The sets and parameters that are automatically updated when displayed on a page

    :attribute Index: (:aimms:index:`IndexCurrentAutoUpdatedDefinitions`)


    .. aimms:index:: IndexCurrentAutoUpdatedDefinitions

        :attribute Range: :aimms:set:`CurrentAutoUpdatedDefinitions`


.. aimms:stringparameter:: CurrentErrorMessage

    :attribute Text: The most recent error message constructed by an external function or procedure


.. aimms:elementparameter:: CurrentFile

    :attribute Text: The file that is currently open or to be opened

    :attribute Range: :aimms:set:`AllFiles`


.. aimms:stringparameter:: CurrentFileName

    :attribute Text: The name of the currently open file


.. aimms:elementparameter:: CurrentGeneratedMathematicalProgram

    :attribute Text: The most recent GeneratedMathematicalProgram generated or modified

    :attribute Range: :aimms:set:`AllGeneratedMathematicalPrograms`


.. aimms:set:: CurrentInputs

    :attribute SubsetOf: :aimms:set:`AllUpdatableIdentifiers`

    :attribute Text: The identifiers that are currently modifiable

    :attribute Index: (:aimms:index:`IndexCurrentInputs`)


    .. aimms:index:: IndexCurrentInputs

        :attribute Range: :aimms:set:`CurrentInputs`


.. aimms:parameter:: CurrentPageNumber

    :attribute Text: The page number of the reporting page currently being printed


.. aimms:parameter:: CurrentMatrixBlockSizes

    :attribute IndexDomain: (:aimms:index:`IndexVariablesConstraints`, :aimms:index:`IndexVariables`)

    :attribute Text: The number of nonzeros in the intersection of a constraint and a variable in the most recently generated matrix


.. aimms:stringparameter:: ODBCDateTimeFormat

    :attribute IndexDomain: (:aimms:index:`IndexIdentifiers`)

    :attribute Text: The format used when reading dates from a database


.. aimms:parameter:: CurrentMatrixRowCount

    :attribute IndexDomain: (:aimms:index:`IndexVariablesConstraints`)

    :attribute Text: The number of rows per constraint in the most recently generated matrix


.. aimms:parameter:: CurrentMatrixColumnCount

    :attribute IndexDomain: (:aimms:index:`IndexVariables`)

    :attribute Text: The number of columns per variable in the most recently generated matrix



.. aimms:set:: AllCases

    :attribute SubsetOf: :aimms:set:`AllDataFiles`

    :attribute Text: All available cases

    :attribute Index: (:aimms:index:`IndexCases`)


    .. aimms:index:: IndexCases

        :attribute Range: :aimms:set:`AllCases`


.. aimms:set:: AllCaseTypes

    :attribute Text: All available case types

    :attribute Index: (:aimms:index:`IndexCaseTypes`)


    .. aimms:index:: IndexCaseTypes

        :attribute Range: :aimms:set:`AllCaseTypes`


.. aimms:set:: AllDataCategories

    :attribute Text: All available data categories

    :attribute Index: (:aimms:index:`IndexDataCategories`)


    .. aimms:index:: IndexDataCategories

        :attribute Range: :aimms:set:`AllDataCategories`


.. aimms:set:: AllDataFiles

    :attribute SubsetOf: :aimms:set:`Integers`

    :attribute Text: All data files present within this project

    :attribute Index: (:aimms:index:`IndexDataFiles`)


    .. aimms:index:: IndexDataFiles

        :attribute Range: :aimms:set:`AllDataFiles`


.. aimms:set:: AllDataSets

    :attribute SubsetOf: :aimms:set:`AllDataFiles`

    :attribute Text: All available data sets

    :attribute Index: (:aimms:index:`IndexDataSets`)


    .. aimms:index:: IndexDataSets

        :attribute Range: :aimms:set:`AllDataSets`


.. aimms:elementparameter:: CurrentCase

    :attribute Text: The current case

    :attribute Range: :aimms:set:`AllCases`


.. aimms:set:: CurrentCaseSelection

    :attribute SubsetOf: :aimms:set:`AllCases`

    :attribute Text: The set of cases selected from within the GUI

    :attribute Index: (:aimms:index:`IndexCurrentCaseSelection`)


    .. aimms:index:: IndexCurrentCaseSelection

        :attribute Range: :aimms:set:`CurrentCaseSelection`


.. aimms:elementparameter:: CurrentDataset

    :attribute IndexDomain: (:aimms:index:`IndexDataCategories`)

    :attribute Text: For each data category the current instance

    :attribute Range: :aimms:set:`AllDataSets`


.. aimms:stringparameter:: DataManagementMonitorID

    :attribute Text: The default ID to check the data changed status in calls to DataChangeMonitor functions.


.. aimms:elementparameter:: CurrentCaseFileContentType

    :attribute Text: The content type that corresponds to the CurrentCase. Used to check whether data has changed and needs saving.

    :attribute Range: :aimms:set:`AllCaseFileContentTypes`


.. aimms:set:: AllCaseFileContentTypes

    :attribute SubsetOf: :aimms:set:`AllSubsetsOfAllIdentifiers`

    :attribute Text: All subsets that can be used to save case files.

    :attribute Index: (:aimms:index:`IndexCaseFileContentTypes`)


    .. aimms:index:: IndexCaseFileContentTypes

        :attribute Range: :aimms:set:`AllCaseFileContentTypes`


.. aimms:stringparameter:: CaseFileURL

    :attribute IndexDomain: (:aimms:index:`IndexCases`)

    :attribute Text: The url of each element in AllCases



.. aimms:set:: AllAbbrMonths

    :attribute Text: Abbreviated month names in English

    :attribute Index: (:aimms:index:`IndexAbbrMonths`)


    .. aimms:index:: IndexAbbrMonths

        :attribute Range: :aimms:set:`AllAbbrMonths`


.. aimms:set:: AllAbbrWeekdays

    :attribute Text: Abbreviated days of the week in English

    :attribute Index: (:aimms:index:`IndexAbbrWeekdays`)


    .. aimms:index:: IndexAbbrWeekdays

        :attribute Range: :aimms:set:`AllAbbrWeekdays`


.. aimms:set:: AllMonths

    :attribute Text: The month names in English

    :attribute Index: (:aimms:index:`IndexMonths`)


    .. aimms:index:: IndexMonths

        :attribute Range: :aimms:set:`AllMonths`


.. aimms:set:: AllTimeZones

    :attribute Text: All TimeZones known to AIMMS

    :attribute Index: (:aimms:index:`IndexTimeZones`)


    .. aimms:index:: IndexTimeZones

        :attribute Range: :aimms:set:`AllTimeZones`


.. aimms:set:: AllWeekdays

    :attribute Text: Days of the week in English

    :attribute Index: (:aimms:index:`IndexWeekdays`)


    .. aimms:index:: IndexWeekdays

        :attribute Range: :aimms:set:`AllWeekdays`


.. aimms:set:: LocaleAllAbbrMonths

    :attribute Text: Abbreviated month names in the local language

    :attribute Index: (:aimms:index:`LocaleIndexAbbrMonths`)


    .. aimms:index:: LocaleIndexAbbrMonths

        :attribute Range: :aimms:set:`LocaleAllAbbrMonths`


.. aimms:set:: LocaleAllAbbrWeekdays

    :attribute Text: Abbreviated days of the week in the local language

    :attribute Index: (:aimms:index:`LocaleIndexAbbrWeekdays`)


    .. aimms:index:: LocaleIndexAbbrWeekdays

        :attribute Range: :aimms:set:`LocaleAllAbbrWeekdays`


.. aimms:set:: LocaleAllMonths

    :attribute Text: Month names in the local language

    :attribute Index: (:aimms:index:`LocaleIndexMonths`)


    .. aimms:index:: LocaleIndexMonths

        :attribute Range: :aimms:set:`LocaleAllMonths`


.. aimms:set:: LocaleAllWeekdays

    :attribute Text: Days of the week in the local language

    :attribute Index: (:aimms:index:`LocaleIndexWeekdays`)


    .. aimms:index:: LocaleIndexWeekdays

        :attribute Range: :aimms:set:`LocaleAllWeekdays`


.. aimms:stringparameter:: LocaleLongDateFormat

    :attribute Text: The long date format according to the current locale


.. aimms:stringparameter:: LocaleShortDateFormat

    :attribute Text: The short date format according to the current locale


.. aimms:stringparameter:: LocaleTimeFormat

    :attribute Text: The time format according to the current locale


.. aimms:stringparameter:: LocaleTimeZoneName

    :attribute IndexDomain: (:aimms:index:`IndexTimeZones`)

    :attribute Text: The local names of the standard time zones


.. aimms:stringparameter:: LocaleTimeZoneNameDST

    :attribute IndexDomain: (:aimms:index:`IndexTimeZones`)

    :attribute Text: The local names of the daylight saving time zones



Module ConstraintProgramming
****************************

.. aimms:librarymodule:: ConstraintProgramming

    :attribute Prefix: ``cp``

.. aimms:module:: cp



Module ErrorHandling
********************

.. aimms:librarymodule:: ErrorHandling

    :attribute Prefix: ``errh``

.. aimms:module:: errh



.. aimms:set:: AllErrorSeverities

    :attribute Text: The severity of situations that can't be handled or are worth further investigation

    :attribute Index: (:aimms:index:`IndexErrorSeverities`)


    .. aimms:index:: IndexErrorSeverities

        :attribute Range: :aimms:set:`AllErrorSeverities`


.. aimms:set:: AllErrorCategories

    :attribute Text: The categories of errors

    :attribute Index: (:aimms:index:`IndexErrorCategories`)


    .. aimms:index:: IndexErrorCategories

        :attribute Range: :aimms:set:`AllErrorCategories`


.. aimms:set:: ErrorCodes

    :attribute Text: The error codes encountered during this session

    :attribute Index: (:aimms:index:`IndexErrorCodes`)


    .. aimms:index:: IndexErrorCodes

        :attribute Range: :aimms:set:`ErrorCodes`


.. aimms:set:: PendingErrors

    :attribute SubsetOf: :aimms:set:`Integers`

    :attribute Text: The errors pending to be handled in topmost error handler

    :attribute Index: (:aimms:index:`IndexPendingErrors`)


    .. aimms:index:: IndexPendingErrors

        :attribute Range: :aimms:set:`PendingErrors`

